const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const indexRoute = require('./routes/indexRoute');
const dietPlanRoute = require('./routes/dietplanRoute');
const userRoute = require('./routes/userRoute');
const recipeRoute = require('./routes/recipeRoute');

mongoose.connect("mongodb://localhost:27017/kalorific", {
  useNewUrlParser: true
});

// use the following code on any request that matches the specified mount path
app.use((req, res, next) => {
  console.log('Recieved new request');
  res.setHeader('Access-Control-Allow-Origin', '*'); //can connect from any host
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE'); //allowable methods
  res.setHeader('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept');
  next();
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}));

// parse application/json
app.use(bodyParser.json());

// add routes
app.use('/', indexRoute);
app.use('/dietPlan', dietPlanRoute);
app.use('/user', userRoute);
app.use('/recipe', recipeRoute);

//to use this middleware in other parts of the application
module.exports = app;