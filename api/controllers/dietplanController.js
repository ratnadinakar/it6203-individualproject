var Dietplan = require('../models/dietPlan');

const user = {
  userID: "67d21577-7e37-4d1c-9591-e5afe256025f"
};

//Get Diet Plan of User
exports.getDietPlan = function (req, res) {
  Dietplan.find(user, function (err, dietPlans) {
    if (err) {
      handleError(err, req, res);
    } else {
      console.log(dietPlans.length);
      var dietPlan = dietPlans[0];
      var respData = {
        status: 'success',
        message: 'diet plan fetched successfully',
        data: dietPlan
      };
      res.status(200).json(respData);
    }
  });
};

//Create Diet Plan for User
exports.createDietPlan = function (req, res) {

  var dietPlanReq = req.body;

  var status = 201;
  var data = {};

  var newDietPlan = new Dietplan({
    userID: user.userID,
    currentWeight: dietPlanReq.currentWeight,
    targetWeight: dietPlanReq.targetWeight,
    preferredMeals: dietPlanReq.preferredMeals,
    allergens: dietPlanReq.allergens,
    preferredDiet: dietPlanReq.preferredDiet,
    targetDate: dietPlanReq.targetDate
  });

  //First Check if a plan exists for the given user
  Dietplan.find(user,
    function (err, dietPlans) {
      if (err) {
        handleError(err, req, res);
      } else {
        if (dietPlans && dietPlans.length == 0) {
          console.log(dietPlans.length);
          //Create New Diet Plan
          saveDietPlan(newDietPlan, req, res);
        } else {
          //Update Existing Diet Plan
          module.exports.updateDietPlan(req,res);
        }
      }
    }
  );
};

function saveDietPlan(newDietPlan, req, res) {
  newDietPlan.save(function (err, savedDietPlan) {
    if (err) {
      handleError(err, req, res);
    } else {
      data = {
        'status': 'success',
        'message': 'diet plan created successfully'
      };
      res.status(200).json(data);
    }
  });
}

function handleError(err, req, res) {
  console.log(err);
  data = {
    'status': 'failure',
    'message': 'unable to create diet plan'
  };
  res.status(500).json(data);
}

//Delete Diet Plan for User
exports.deleteDietPlan = function (req, res) {
  Dietplan.findOneAndDelete(user, function (err, deletedDietPlan) {

    if (err) {
      handleError(err, req, res);
    } else {
      data = {
        'status': 'success',
        'message': 'deleted diet plan'
      };
      res.status(200).json(data);
    }
  }); //End findOneAndDelete
};

//Updates Diet Plan for User
exports.updateDietPlan = function (req,res) {

  var dietPlanReq = req.body;

  var status = 201;
  var data = {};

  var updatedDietPlan = {
    userID: user.userID,
    currentWeight: dietPlanReq.currentWeight,
    targetWeight: dietPlanReq.targetWeight,
    preferredMeals: dietPlanReq.preferredMeals,
    allergens: dietPlanReq.allergens,
    preferredDiet: dietPlanReq.preferredDiet,
    targetDate: dietPlanReq.targetDate
  };

  Dietplan.findOneAndUpdate(user,updatedDietPlan,{upsert: true, new: true, runValidators: true},function(err,dbUpdatedDietPlan){

    if (err) {
      handleError(err, req, res);
    } else {
      data = {
        'status': 'success',
        'message': 'updated diet plan successfully'
      };
      res.status(200).json(data);
    }

  });//End of findOneAndUpdate
}