const mongoose = require('mongoose');

var Recipe = require('../models/recipe');

function handleError(err, req, res) {
    console.log(err);
    data = {
        'status': 'failure',
        'message': 'error with the recipe operation'
    };
    res.status(500).json(data);
}

//Function Copied from 
//https://gist.github.com/pilgreen/72be9f2964caa8030592
function guid() {
    var grn = Math.round(Math.random() * 10);
    var nsu = 'xxxxxxxx-xxxx-' + grn + 'xxx-yxxx-xxxxxxxxxxxx'
    return nsu.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

exports.getRecipe = function (req, res) {
    //call mongoose method find (MongoDB db.Students.find())
    Recipe.find()
        //if data is returned, send data as a response 
        .then(data => {
            var respData = {
                status: 'success',
                message: 'Recipe was fetched successfully!',
                data: data
            };
            res.status(200).json(respData);
        })
        //if error, send internal server error
        .catch(err => {
            console.log('Error: ${err}');
            handleError(err, req, res);
        });
};

exports.saveRecipe = function (req, res) {
    // create a new student variable and save request’s fields 
    const recipe = new Recipe({
        recipeName: req.body.recipeName,
        items: req.body.items,
        directions: req.body.directions
    });
    //send the document to the database 
    recipe.save()
        //in case of success
        .then(() => {
            var respData = {
                status: 'success',
                message: 'Recipe was saved successfully!'
            };
            res.status(200).json(respData);
        })
        //if error
        .catch(err => {
            console.log('Error: ${err}');
            handleError(err, req, res);
        });
};

exports.updateRecipe = function (req, res) {
    var recipeID = req.params.id;
    console.log("id:" + recipeID)
    // check that the parameter id is valid 
    if (mongoose.Types.ObjectId.isValid(recipeID)) {
        //find a document and set new first and last names
        console.log("id:" + recipeID);
        var updatedRecipe = {
            recipeName: req.body.recipeName,
            items: req.body.items,
            directions: req.body.directions
        };
        Recipe.findOneAndUpdate({
                    _id: recipeID
                },
                updatedRecipe, {
                    upsert: true,
                    new: true,
                    runValidators: true
                })
            .then((Recipe) => {
                if (Recipe) { //what was updated
                    console.log("id:" + recipeID);
                    console.log(Recipe);
                    var respData = {
                        status: 'success',
                        message: 'Recipe was updated successfully!'
                    };
                    res.status(200).json(respData);
                } else {
                    console.log("no data exist for this id");
                    var respData = {
                        status: 'failure',
                        message: 'Recipe was not updated as it was not found!'
                    };
                    res.status(404).json(respData);
                }
            })
            .catch((err) => {
                console.log('Error: ${err}');
                handleError(err, req, res);
            });
    } else {
        console.log("please provide correct id");
        var respData = {
            status: 'failure',
            message: 'Invalid ID'
        };
        res.status(400).json(respData);
    }
}

exports.deleteRecipe = function (req, res) {
    Recipe.deleteOne({
            _id: req.params.id
        })
        .then(result => {
            console.log(result);
            var respData = {
                status: 'success',
                message: 'Recipe was deleted successfully!'
            };
            res.status(200).json(respData);
        }).catch((err) => {
            console.log('Error: ${err}');
            handleError(err, req, res);
        });
}