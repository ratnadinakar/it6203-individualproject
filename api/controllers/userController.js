var User = require('../models/user');

function handleError(err, req, res) {
    console.log(err);
    data = {
        'status': 'failure',
        'message': 'error with the user operation'
    };
    res.status(500).json(data);
}

//Function Copied from 
//https://gist.github.com/pilgreen/72be9f2964caa8030592
function guid() {
    var grn = Math.round(Math.random() * 10);
    var nsu = 'xxxxxxxx-xxxx-' + grn + 'xxx-yxxx-xxxxxxxxxxxx'
    return nsu.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

exports.validateLogin = function (req, res) {

    var login = req.body;

    var credentials = {
        email: login.email,
        password: login.password
    };

    User.findOne()
        .where('email').equals(login.email)
        .where('password').equals(login.password)
        .where('locked').equals(false)
        .select('firstName lastName userID')
        .exec(function (err, user) {
            if (err) {
                handleError(err, req, res);
            } else {
                console.log(user);
                var respData = {
                    status: 'success',
                    message: 'user validate and data fetched successfully',
                    data: user
                };
                res.status(200).json(respData);
            }
        });
};

exports.addUser = function (req, res) {

    var userReq = req.body;

    var newUser = new User({
        userID: guid(),
        firstName: userReq.firstName,
        lastName: userReq.lastName,
        email: userReq.email,
        password: userReq.password,
        gender: userReq.gender,
        dateOfBirth: userReq.dateOfBirth
    });

    newUser.save(function (err, user) {
        if (err) {
            handleError(err, req, res);
        } else {
            data = {
                'status': 'success',
                'message': 'user registered successfully'
            };
            res.status(200).json(data);
        }
    });

};