const mongoose = require('mongoose');

const dietplanSchema = mongoose.Schema({
  userID: {
    type: String,
    required: true
  },
  currentWeight: {
    type: Number,
    required: true
  },
  targetWeight: {
    type: Number,
    required: true
  },
  preferredDiet: {
    type: String,
    required: true
  },
  preferredMeals: {
    type: [String],
    required: true
  },
  targetDate: {
    type: Date,
    required: true
  },
  allergens: {
    type: [String],
    required: true
  },
  dateCreated: {
    type: Date,
    default: Date.now
  },
  dateModified: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Dietplan', dietplanSchema, 'Dietplans');
