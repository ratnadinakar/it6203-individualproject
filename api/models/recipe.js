const mongoose = require('mongoose');

//define a schema/ blueprint NOTE: id is not a part of the schema 
const recipeSchema = mongoose.Schema({
  recipeName: {
    type: String,
    required: true,
    index: true,
    unique: true
  },
  items: {
    type: String,
    required: true
  },
  directions: {
    type: String,
    required: true
  }
});

//use the blueprint to create the model 
// Parameters: (model_name, schema_to_use, collection_name)
//module.exports is used to allow external access to the model  
module.exports = mongoose.model('Recipe', recipeSchema, 'Recipes');
//note capital S in the collection name