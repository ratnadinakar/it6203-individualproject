var express = require('express');
var router = express.Router();

//Dietplan Controller
var dietplanController = require('../controllers/dietplanController');

//Routes
//R: GET : Fetch Diet Plan for user
router.get('/', dietplanController.getDietPlan);

//C: POST : Create a new Diet Plan for user
router.post('/', dietplanController.createDietPlan);

//D: DELETE : Delete existing Diet plan for user
router.delete('/', dietplanController.deleteDietPlan);

//U: UPDATE : Update Existing Diet Plan for user
router.put('/', dietplanController.updateDietPlan);

module.exports = router;
