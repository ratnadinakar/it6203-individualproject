var express = require('express');
var router = express.Router();

//Recipe Controller
var recipeController = require('../controllers/recipeController');

//Routes
//R: GET : Fetch Diet Plan for user
router.get('/', recipeController.getRecipe);

//C: POST : Create a new Diet Plan for user
router.post('/', recipeController.saveRecipe);

//D: DELETE : Delete existing Diet plan for user
router.delete('/:id', recipeController.deleteRecipe);

//U: UPDATE : Update Existing Diet Plan for user
router.put('/:id', recipeController.updateRecipe);

module.exports = router;
