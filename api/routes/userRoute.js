var express = require('express');
var router = express.Router();

//User Controller
var userController = require('../controllers/userController');

//Routes

//R: POST : Validate Login Credentials and Return Data
router.post('/login', userController.validateLogin);

//C: POST : Add New User
router.post('/', userController.addUser);

module.exports = router;
