import { RecipeService } from './recipe.service';
import { DietPlanService } from './dietplan.service';
import { UserService } from './user.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { MaterialModule } from './material/material.module';

import { AppComponent } from './app.component';
import { MainToolbarComponent } from './main-toolbar/main-toolbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DietPlanInfoComponent } from './diet-plan-info/diet-plan-info.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DietPlanFormComponent } from './diet-plan-form/diet-plan-form.component';
import { MealPlanFormComponent } from './meal-plan-form/meal-plan-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { PasswordResetFormComponent } from './password-reset-form/password-reset-form.component';
import { ListRecipeComponent } from './list-recipe/list-recipe.component';
import { NewRecipeComponent } from './new-recipe/new-recipe.component';
import { IndexComponent } from './index/index.component';
import { LogoutComponent } from './logout/logout.component';


const appRoutes: Routes = [
  { path: 'dashBoard', component: DashboardComponent },
  { path: 'dietPlanForm', component: DietPlanFormComponent },
  { path: 'mealPlanForm', component: MealPlanFormComponent },
  { path: 'index', component: IndexComponent },
  { path: 'registrationForm', component: RegistrationFormComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'listRecipe', component: ListRecipeComponent },
  { path: 'newRecipe', component: NewRecipeComponent },
  { path: 'editRecipe/:_id', component: NewRecipeComponent },
  { path: '', redirectTo: '/index', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MainToolbarComponent,
    DashboardComponent,
    DietPlanInfoComponent,
    PageNotFoundComponent,
    DietPlanFormComponent,
    MealPlanFormComponent,
    LoginFormComponent,
    RegistrationFormComponent,
    PasswordResetFormComponent,
    ListRecipeComponent,
    NewRecipeComponent,
    IndexComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [DietPlanService, UserService, RecipeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
