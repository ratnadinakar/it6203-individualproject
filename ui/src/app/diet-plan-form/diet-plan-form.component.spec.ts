import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietPlanFormComponent } from './diet-plan-form.component';

describe('DietPlanFormComponent', () => {
  let component: DietPlanFormComponent;
  let fixture: ComponentFixture<DietPlanFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietPlanFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietPlanFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
