import { Component, OnInit, Input } from "@angular/core";
import { DietPlan } from "./../dietplan";
import { DietPlanService } from "./../dietplan.service";

@Component({
  selector: "app-diet-plan-form",
  templateUrl: "./diet-plan-form.component.html",
  styleUrls: ["./diet-plan-form.component.css"]
})
export class DietPlanFormComponent implements OnInit {
  @Input()
  currentWeight: number;
  @Input()
  targetWeight: number;
  @Input()
  preferredDiet: string;
  @Input()
  preferredMeals: Array<string> = [];
  @Input()
  targetDate: string;
  @Input()
  allergens: Array<string> = [];

  dietPlan: DietPlan;

  actionBanner: String;
  createDietPlan: boolean;
  editDietPlan: boolean;

  constructor(private _dietPlanService: DietPlanService) {

    this.actionBanner = 'New ';
    this.createDietPlan = true;
    this.editDietPlan = false;

    this._dietPlanService.getDietPlan().subscribe(responseData => {
      const savedDietPlan = responseData["data"];
      if (savedDietPlan) {
        this.dietPlan = new DietPlan(
          savedDietPlan.currentWeight,
          savedDietPlan.targetWeight,
          savedDietPlan.preferredDiet,
          savedDietPlan.preferredMeals,
          savedDietPlan.targetDate,
          savedDietPlan.allergens
        );

        if (this.dietPlan) {
          //Populate form with existing diet plan
          this.currentWeight = this.dietPlan.currentWeight;
          this.targetWeight = this.dietPlan.targetWeight;
          this.preferredDiet = this.dietPlan.preferredDiet;
          this.targetDate = this.dietPlan.targetDate;
          this.preferredMeals = this.dietPlan.preferredMeals;
          this.allergens = this.dietPlan.allergens;
          this.actionBanner = 'View / Modify';
          this.createDietPlan = false;
          this.editDietPlan = true;
        }
      }
    });
  }

  addPreferredMeal(meal: string) {
    if (this.preferredMeals.indexOf(meal) === -1) {
      this.preferredMeals.push(meal);
    }
  }

  addAllergen(allergen: string) {
    if (this.allergens.indexOf(allergen) === -1) {
      this.allergens.push(allergen);
    }
  }

  onSubmit() {
    const dietPlan = new DietPlan(
      this.currentWeight,
      this.targetWeight,
      this.preferredDiet,
      this.preferredMeals,
      this.targetDate,
      this.allergens
    );

    this._dietPlanService.addDietPlan(dietPlan);
  }

  deleteDietplan() {
    this._dietPlanService.deleteDietPlan();
  }

  ngOnInit() {}
}
