import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietPlanInfoComponent } from './diet-plan-info.component';

describe('DietPlanInfoComponent', () => {
  let component: DietPlanInfoComponent;
  let fixture: ComponentFixture<DietPlanInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietPlanInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietPlanInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
