import { Component, OnInit } from "@angular/core";
import { DietPlan } from "./../dietplan";
import { DietPlanService } from "./../dietplan.service";

@Component({
  selector: "app-diet-plan-info",
  templateUrl: "./diet-plan-info.component.html",
  styleUrls: ["./diet-plan-info.component.css"]
})
export class DietPlanInfoComponent implements OnInit {
  newDietPlan: boolean;
  viewDietPlan: boolean;
  goal: String;
  actionBanner: String;
  dietPlan: DietPlan;

  constructor(private _dietPlanService: DietPlanService) {

    this.newDietPlan = true;
    this.viewDietPlan = false;
    this.goal  = 'Lose / Gain Weight';
    this.actionBanner = 'Create Diet Plan';

    this._dietPlanService.getDietPlan().subscribe(responseData => {
      const savedDietPlan = responseData["data"];
      if (savedDietPlan) {
        this.dietPlan = new DietPlan(
          savedDietPlan.currentWeight,
          savedDietPlan.targetWeight,
          savedDietPlan.preferredDiet,
          savedDietPlan.preferredMeals,
          savedDietPlan.targetDate,
          savedDietPlan.allergens
        );

        if (this.dietPlan) {
          this.newDietPlan = false;
          this.viewDietPlan = true;
          this.goal =
           this. dietPlan.targetWeight < this.dietPlan.currentWeight
              ? "Lose Weight"
              : "Gain Weight";
          this.actionBanner = 'View / Modify Diet Plan';
        } else {
          this.newDietPlan = true;
          this.viewDietPlan = false;
          this.goal = 'Lose / Gain Weight';
          this.actionBanner = 'Create Diet Plan';
        }
      }
    });

  }

  ngOnInit() {}

}
