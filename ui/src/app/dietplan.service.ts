import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { DietPlan } from "./dietplan";
import { Observable } from "rxjs";
import { environment } from "./../environments/environment";

//we know that response will be in JSON format
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class DietPlanService {
  private dietPlan;

  constructor(private http: HttpClient) {}

  addDietPlan(dietPlan: DietPlan) {
    this.http
      .post("http://" + environment.appURL + "/dietPlan", dietPlan)
      .subscribe(responseData => {
        console.log(responseData);
      });
  }

  deleteDietPlan() {
    this.http
      .delete("http://" + environment.appURL + "/dietPlan")
      .subscribe(responseData => {
        console.log(responseData);
      });
  }

  getDietPlan() {
    return this.http.get("http://" + environment.appURL + "/dietPlan");
  }
}
