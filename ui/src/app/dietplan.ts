export class DietPlan {
  constructor(
    public currentWeight: number,
    public targetWeight: number,
    public preferredDiet: string,
    public preferredMeals: Array<string>,
    public targetDate: string,
    public allergens: Array<string>
  ) {}
}
