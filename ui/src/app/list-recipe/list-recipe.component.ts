import { Component, OnInit } from '@angular/core';
import { RecipeService } from './../recipe.service';

@Component({
  selector: 'app-list-recipe',
  templateUrl: './list-recipe.component.html',
  styleUrls: ['./list-recipe.component.css']
})
export class ListRecipeComponent implements OnInit {

  title ='Recipe';
  public recipes;
  //initialize the call using RecipeService 
  constructor(private _myService: RecipeService) { }
  ngOnInit() {
    this.getRecipes();
  }
  //method called OnInit
  getRecipes() {
   this._myService.getRecipes().subscribe(
      //read data and assign to public variable recipes
      data => { this.recipes = data['data']},
      err => console.error(err),
      () => console.log('finished loading')
    );
  }
  onDelete(studentId: string) {
    this._myService.deleteRecipe(studentId);
  }

}
