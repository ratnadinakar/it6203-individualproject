import { Component, OnInit, Input } from "@angular/core";
import { UserService } from "./../user.service";
import { User } from "./../user";
import { Router } from "@angular/router";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.css"]
})
export class LoginFormComponent implements OnInit {
  @Input()
  email: string;
  @Input()
  password: string;

  constructor(private router: Router, private _userService: UserService) {}

  ngOnInit() {}

  onSubmit() {
    const userCredentials = new User("", "", "", "", this.email, this.password);

    this._userService.userLogin(userCredentials).subscribe(
      responseData => {
        const userData = responseData["data"];
        if (userData) {
          const user = new User(
            userData["firstName"],
            userData["lastName"],
            "",
            "",
            this.email,
            ""
          );
          localStorage.setItem("loggedUser", JSON.stringify(user));
          localStorage.setItem("isLoggedIn", "true");
          this.router.navigate(["/dashBoard"]);
          location.reload(true);
        }
      },
      error => {
        console.log(error);
      }
    );
  }
}
