import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-toolbar',
  templateUrl: './main-toolbar.component.html',
  styleUrls: ['./main-toolbar.component.css']
})
export class MainToolbarComponent implements OnInit {

  isLoggedIn = false;
  firstName = 'Valuable';
  lastName = 'User';

  constructor() {
    this.isLoggedIn = ('true' === localStorage.getItem('isLoggedIn'));

    if (this.isLoggedIn) {
      const user = JSON.parse(localStorage.getItem('loggedUser'));

      this.firstName = user.firstName;
      this.lastName = user.lastName;
    }
  }

  ngOnInit() {
  }

}
