import { Component, OnInit, Input } from '@angular/core';
import { RecipeService } from '../recipe.service';
import { Router } from '@angular/router';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.css']
})
export class NewRecipeComponent implements OnInit {
  @Input() recipeName: string;
  @Input() items: string;
  @Input() directions: string;
  mode = 'add'; //default mode
  id: string; //Recipe ID

  constructor(private _myService: RecipeService, private router: Router,
    public route: ActivatedRoute) { }

  onSubmit() {
    if (this.mode == 'add') {
      console.log("You submitted: " + this.recipeName + " " + this.items + " " + this.directions);
      this._myService.addRecipe(this.recipeName, this.items, this.directions);
      this.router.navigate(['/listRecipe']);
    } if (this.mode == 'edit') {
      console.log("You updated: " + this.id + " " + this.recipeName + " " + this.items + " " + this.directions);
      this._myService.updateRecipe(this.id, this.recipeName, this.items, this.directions);
      this.router.navigate(['/listRecipe']);
    }
  }
  ngOnInit() {
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('_id')) {
      this.mode = 'edit'; /*request had a parameter _id */
        this.id = paramMap.get('_id');
      }
      else {
      this.mode = 'add';
        this.id = null;
      }
    });
  }
}
