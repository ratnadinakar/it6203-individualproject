import { Recipe } from "./recipe";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "./../environments/environment";

//we know that response will be in JSON format
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable()
export class RecipeService {
  constructor(private http: HttpClient) {}

  // Uses http.get() to load data
  getRecipes() {
    return this.http.get("http://" + environment.appURL + "/recipe");
  }
  // Uses http.post() to post data
  addRecipe(recipeName: string, items: string, directions: string) {
    this.http
      .post("http://" + environment.appURL + "/recipe", { recipeName, items, directions })
      .subscribe(responseData => {
        console.log(responseData);
      });
    location.reload();
  }
  updateRecipe(
    recipeId: string,
    recipeName: string,
    items: string,
    directions: string
  ) {
    //request path http://localhost:8000/students/5xbd456xx
    //first and last names will be send as HTTP body parameters
    this.http
      .put("http://" + environment.appURL + "/recipe/" + recipeId, {
        recipeName,
        items,
        directions
      })
      .subscribe(() => {
        console.log("Updated: " + recipeId);
      });
    location.reload();
  }
  deleteRecipe(recipeId: string) {
    this.http
      .delete("http://" + environment.appURL + "/recipe/" + recipeId)
      .subscribe(() => {
        console.log("Deleted: " + recipeId);
      });
    location.reload();
  }
}
