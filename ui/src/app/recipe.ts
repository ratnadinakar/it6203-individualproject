export class Recipe {
  constructor(
    public recipeName: string,
    public items: string,
    public directions: string
  ) {}
}
