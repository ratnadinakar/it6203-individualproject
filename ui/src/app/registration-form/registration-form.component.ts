import { Component, OnInit, Input } from '@angular/core';
import { UserService } from './../user.service';
import { User } from './../user';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
  @Input()
  firstName: string;
  @Input()
  lastName: string;
  @Input()
  gender: string;
  @Input()
  dateOfBirth: string;
  @Input()
  email: string;
  @Input()
  password: string;
  @Input()
  confirmPassword: string;

  maxDateOfBirth = new Date();

  constructor(private _userService: UserService) { }

  ngOnInit() {
  }

  onSubmit() {
    const newUser = new User(this.firstName,
      this.lastName,
      this.gender,
      this.dateOfBirth,
      this.email,
      this.password);

    this._userService.registerUser(newUser)
      .subscribe(
        responseData => { console.log(responseData); }
        , error => { console.log(error); }
      );
  }

}
