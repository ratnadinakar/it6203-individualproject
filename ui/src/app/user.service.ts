import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "./user";
import { Observable } from "rxjs";
import { environment } from "./../environments/environment";

//we know that response will be in JSON format
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(private http: HttpClient) {}

  registerUser(user: User) {
    return this.http.post("http://" + environment.appURL + "/user", user);
  }

  userLogin(user: User) {
    return this.http.post("http://" + environment.appURL + "/user/login", user);
  }

  userLogOut() {}
}
