export class User {
    private _userID: string;
    constructor(
        public firstName: string,
        public lastName: string,
        public gender: string,
        public dateOfBirth: string,
        public email: string,
        public password: string
    ) { }

    get userId(): string {
        return this._userID;
    }

    set userId(pUserId: string) {
        this._userID = pUserId;
    }

}
